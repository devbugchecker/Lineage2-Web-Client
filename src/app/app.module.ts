import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { HttpLoaderFactory } from './app.translate.factory';

import { RouterModule, Routes } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import {CoreModule} from './core/core.module';

import { AppComponent } from './core/containers/app';
import { NotFoundComponent } from './core/containers/not-found';

import { DonationsComponent } from './donations/components/donations';

import { OlympiadsComponent } from './olympiads/components/olympiads';
import { OlympiadsHeroesComponent } from './olympiads/containers/olympiads-heroes.component';
import { OlympiadsRankingComponent } from './rankings/components/ranking-oympiad';

import { RankingsComponent } from './rankings/components/rankings';
import { RankingCharactersComponent } from './rankings/components/ranking-characters';
import { RankingClansComponent } from './rankings/components/ranking-clans';

import { UserComponent } from './user/components/user';

import { AccountModule } from './account/account.module';
import { DownloadsModule } from './downloads/downloads.module';
import { InformationsModule } from './informations/informations.module';
import {CastleSiegeModule} from './castle-siege/castle-siege.module';
import {ContactModule} from './contact/contact.module';


const appRoutes: Routes = [
{ path: '', component: UserComponent, pathMatch: 'full' },

  { path: 'olimpiadas', component: OlympiadsComponent },

  { path: 'rankings/characters', component: RankingCharactersComponent },
  { path: 'rankings/clans', component: RankingClansComponent },
  { path: 'rankings/olimpiadas', component: OlympiadsRankingComponent },


  { path: 'usuario', component: UserComponent },

  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgbModule.forRoot(),
    CoreModule,
    AccountModule,
    DownloadsModule,
    InformationsModule,
    CastleSiegeModule,
    ContactModule
  ],
  declarations: [
                  DonationsComponent,
                  OlympiadsComponent, OlympiadsHeroesComponent, OlympiadsRankingComponent,
                  RankingsComponent, RankingCharactersComponent, RankingClansComponent,
                  UserComponent
                  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
