import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthModule } from '../auth/auth.module';
import { SharedModule } from '../shared/shared.module';

import { AppComponent } from './containers/app';
import { NotFoundComponent } from './containers/not-found';

import { TemplateDefaultComponent } from './templates/default/components/template-default';

export const COMPONENTS = [
  AppComponent,
  NotFoundComponent,
  TemplateDefaultComponent
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AuthModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class CoreModule { }
