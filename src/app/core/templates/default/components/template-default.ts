import {Component, HostListener, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-template-default',
  templateUrl: './template-default.html',
  styleUrls: ['./template-default.css'],
  encapsulation: ViewEncapsulation.None
})
export class TemplateDefaultComponent {}
