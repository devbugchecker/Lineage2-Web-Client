import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-download-edit',
  templateUrl: './download-edit.html'
})
export class DownloadEditComponent implements OnInit{
  editDownload: boolean = false;
  id: number;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.editDownload = params['id'] != null;
      this.id = +params['id'];
    });

  }
}
