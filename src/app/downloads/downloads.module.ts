import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DownloadsRoutingModule } from './downloads-routing.module';

import { DownloadsComponent } from './components/downloads';
import { DownloadsListComponent } from './components/downloads-list';
import { DownloadEditComponent } from './components/download-edit';



@NgModule({

  imports: [
    CommonModule,
    DownloadsRoutingModule
  ],
  declarations: [
    DownloadsComponent,
    DownloadsListComponent,
    DownloadEditComponent
  ]
})
export class DownloadsModule {}
