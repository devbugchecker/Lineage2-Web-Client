import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthLoginComponent } from './components/auth-login';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    AuthLoginComponent
  ],
  exports: [
    AuthLoginComponent
  ]
})
export class AuthModule { }
