import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DonationRoutingModule } from './donation-routing.module';

@NgModule({
  imports: [
    CommonModule,
    DonationRoutingModule
  ],
  declarations: [ ]
})
export class DonationModule { }
